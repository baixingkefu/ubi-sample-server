# Sample Server

This is a sample project demonstrating how to integrate with BXCL UBI OpenAPIs.

## Build

```
mvn clean install
```

## Run this server

Run the following command with `<APPKEY>` and `<APPSECRET>` replaced with the
keys of your own application, the server will be started and listening on port
8001.

```
java -jar target/sample-server-0.0.1-SNAPSHOT.jar --ubi.passport.appkey=<APPKEY> --ubi.passport.appsecret=<APPSECRET>
```
