package com.bxcl.ubi.sampleserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UbiSampleServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(UbiSampleServerApplication.class, args);
	}

}
