package com.bxcl.ubi.sampleserver.common;

public class Result<T> {
    private int code;
    private String message;
    private Object data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "R{" + "code=" + code + ", message='" + message + '\'' + ", data=" + data + '}';
    }

    public static <T> Result<T> success(Object data) {
        Result<T> rb = new Result<T>();
        rb.setCode(1);
        rb.setMessage("OK");
        rb.setData(data);
        return rb;
    }

    public static <T> Result<T> error(int code, String message) {
        Result<T> rb = new Result<T>();
        rb.setCode(code);
        rb.setMessage(message);
        rb.setData(null);
        return rb;
    }
}
