package com.bxcl.ubi.sampleserver.common;

public class ServiceException extends RuntimeException {

    protected int errorCode;
    protected String errorMsg;

    public ServiceException() {
        super();
    }

    public ServiceException(int errorCode, String errorMsg) {
        super(Integer.toString(errorCode));
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public ServiceException(int errorCode, String errorMsg, Throwable cause) {
        super(Integer.toString(errorCode), cause);
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String getMessage() {
        return errorMsg;
    }
}
