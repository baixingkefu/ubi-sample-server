package com.bxcl.ubi.sampleserver.controller;

import com.alibaba.cloudapi.sdk.enums.Scheme;
import com.alibaba.cloudapi.sdk.model.ApiResponse;
import com.alibaba.cloudapi.sdk.model.HttpClientBuilderParams;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bxcl.ubi.sampleserver.client.UBIPassportClient;
import com.bxcl.ubi.sampleserver.common.Result;
import com.bxcl.ubi.sampleserver.common.ServiceException;
import com.bxcl.ubi.sampleserver.entity.AuthRequest;
import com.bxcl.ubi.sampleserver.entity.AuthResponse;
import com.bxcl.ubi.sampleserver.entity.User;
import com.bxcl.ubi.sampleserver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {
    static private final int NO_SUCH_USER = 100;
    static private final int IDENTITY_MISMATCH = 101;
    static private final int UBI_CLIENT_FAILED = 501;

    private final UserRepository repository;
    private final UBIPassportClient ubiPassportClient;

    AuthController(UserRepository repository,
                   @Value("${ubi.passport.server}") String host,
                   @Value("${ubi.passport.appkey}") String appKey,
                   @Value("${ubi.passport.appsecret}") String appSecret) {
        this.repository = repository;
        ubiPassportClient = new UBIPassportClient();
        HttpClientBuilderParams httpsParam = new HttpClientBuilderParams();
        httpsParam.setScheme(Scheme.HTTPS);
        httpsParam.setHost(host);
        httpsParam.setAppKey(appKey);
        httpsParam.setAppSecret(appSecret);
        ubiPassportClient.init(httpsParam);
    }

    @PostMapping("/auth/ubiLogin")
    Result<AuthResponse> ubiLogin(@RequestBody AuthRequest request) {
        User user = getUserFromCredential(request.getCredential());
        Long userId = user.getId();

        // Request UBI Passport for UBI User ID and token
        ApiResponse loginResponse = ubiPassportClient.login(userId);
        JSONObject loginResult = JSON.parseObject(loginResponse.getBody(), JSONObject.class);
        if (loginResult.getInteger("code") != 1) {
            throw new ServiceException(UBI_CLIENT_FAILED, "Failed to request UBI passport");
        }
        Long ubiUserId = loginResult.getJSONObject("data").getLong("userId");
        String ubiToken = loginResult.getJSONObject("data").getString("token");

        // Update UBI User ID
        user.setUbiUserId(ubiUserId);
        repository.save(user);

        AuthResponse response = new AuthResponse();
        response.setUserId(userId);
        response.setUbiUserId(ubiUserId);
        response.setUbiToken(ubiToken);
        return Result.success(response);
    }

    @PostMapping("/auth/renewUbiToken")
    Result<AuthResponse> renewUbiToken(@RequestBody AuthRequest request) {
        Long ubiUserId = request.getUbiUserId();
        if (ubiUserId == null) {
            throw new IllegalArgumentException("ubiUserId should be specified");
        }
        User user = getUserFromCredential(request.getCredential());

        // Check user identity to get rid of impersonation
        if (!user.getUbiUserId().equals(ubiUserId)) {
            throw new ServiceException(IDENTITY_MISMATCH, "ubiUserId does not match user identity");
        }

        // Request UBI Passport for token
        ApiResponse loginResponse = ubiPassportClient.getToken(ubiUserId);
        JSONObject loginResult = JSON.parseObject(loginResponse.getBody(), JSONObject.class);
        if (loginResult.getInteger("code") != 1) {
            throw new ServiceException(UBI_CLIENT_FAILED, "Failed to request UBI passport");
        }
        String ubiToken = loginResult.getJSONObject("data").getString("token");

        AuthResponse response = new AuthResponse();
        response.setUserId(user.getId());
        response.setUbiUserId(ubiUserId);
        response.setUbiToken(ubiToken);
        return Result.success(response);
    }

    private User getUserFromCredential(String cred) {
        // Cred is as simple as user Id in our example
        Long userId = Long.parseLong(cred);
        return repository.findById(userId).orElseThrow(() -> new ServiceException(NO_SUCH_USER, "User not found"));
    }
}
