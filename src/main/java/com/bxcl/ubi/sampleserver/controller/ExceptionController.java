package com.bxcl.ubi.sampleserver.controller;

import com.bxcl.ubi.sampleserver.common.Result;
import com.bxcl.ubi.sampleserver.common.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.ServletException;

@Slf4j
@RestControllerAdvice
public class ExceptionController {

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public Result methodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        return Result.error(HttpStatus.METHOD_NOT_ALLOWED.value(), "Method not allowed");
    }

    @ExceptionHandler(value = HttpMessageConversionException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result messageConversionException(HttpMessageConversionException e) {
        return Result.error(HttpStatus.BAD_REQUEST.value(), "Invalid request");
    }

    @ExceptionHandler(value = ServletException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result servletException(ServletException e) {
        return Result.error(HttpStatus.BAD_REQUEST.value(), "Invalid request");
    }

    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result bindException(BindException e) {
        return Result.error(HttpStatus.BAD_REQUEST.value(), "Invalid request");
    }

    @ExceptionHandler(ServiceException.class)
    public Result runtimeExceptionHandler(ServiceException e) {
        log.error("Caught service exception, code:{},msg:{}", e.getErrorCode(), e.getMessage(), e);
        return Result.error(e.getErrorCode(), e.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public Result illegalArgumentExceptionHandler(RuntimeException e) {
        log.error("Caught IllegalArgumentException", e);
        return Result.error(400, "Illegal argument");
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Result runtimeExceptionHandler(RuntimeException e) {
        log.error("Caught RuntimeException", e);
        return Result.error(500, "Internal runtime error");
    }

    /**
     * 处理其他异常
     *
     * @param e exception
     * @return 响应结果
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public Result exceptionHandler(Exception e) {
        log.error("Caught Exception", e);
        return Result.error(500, "Internal error");
    }
}
