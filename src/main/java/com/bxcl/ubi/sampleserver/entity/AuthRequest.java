package com.bxcl.ubi.sampleserver.entity;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class AuthRequest {
    @NotBlank
    String credential;

    Long ubiUserId;
}
