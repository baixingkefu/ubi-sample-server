package com.bxcl.ubi.sampleserver.entity;

import lombok.Data;

@Data
public class AuthResponse {
    Long userId;
    Long ubiUserId;
    String ubiToken;
}
