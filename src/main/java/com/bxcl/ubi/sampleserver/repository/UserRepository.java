package com.bxcl.ubi.sampleserver.repository;

import com.bxcl.ubi.sampleserver.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
