package com.bxcl.ubi.sampleserver.repository;

import com.bxcl.ubi.sampleserver.entity.User;
import com.bxcl.ubi.sampleserver.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class LoadDatabase {
    @Bean
    CommandLineRunner initDatabase(UserRepository repository) {
        return args -> {
            log.info("Preloading " + repository.save(new User(100001L, "testuser1", 0L)));
            log.info("Preloading " + repository.save(new User(100002L, "testuser2", 0L)));
            log.info("Preloading " + repository.save(new User(100003L, "testuser3", 0L)));
        };
    }
}
