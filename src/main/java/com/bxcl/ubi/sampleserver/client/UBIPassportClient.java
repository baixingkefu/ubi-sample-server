package com.bxcl.ubi.sampleserver.client;

import com.alibaba.cloudapi.sdk.client.ApacheHttpClient;
import com.alibaba.cloudapi.sdk.enums.HttpMethod;
import com.alibaba.cloudapi.sdk.model.ApiRequest;
import com.alibaba.cloudapi.sdk.model.ApiResponse;
import com.alibaba.cloudapi.sdk.model.HttpClientBuilderParams;
import com.alibaba.fastjson.JSON;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class UBIPassportClient extends ApacheHttpClient {

    public void init(HttpClientBuilderParams httpClientBuilderParams) {
        super.init(httpClientBuilderParams);
    }

    public ApiResponse login(Long orgUserId) {
        String path = "/auth/login";
        Map<String, Long> requestParams = new HashMap<>();
        requestParams.put("orgUserId", orgUserId);
        byte[] body = JSON.toJSONString(requestParams).getBytes(StandardCharsets.UTF_8);
        ApiRequest request = new ApiRequest(HttpMethod.POST_BODY, path, body);
        request.addHeader("Content-Type", "application/json");
        return sendSyncRequest(request);
    }

    public ApiResponse getToken(Long ubiUserId) {
        String path = "/auth/getToken";
        Map<String, Long> requestParams = new HashMap<>();
        requestParams.put("userId", ubiUserId);
        byte[] body = JSON.toJSONString(requestParams).getBytes(StandardCharsets.UTF_8);
        ApiRequest request = new ApiRequest(HttpMethod.POST_BODY, path, body);
        request.addHeader("Content-Type", "application/json");
        return sendSyncRequest(request);
    }
}
