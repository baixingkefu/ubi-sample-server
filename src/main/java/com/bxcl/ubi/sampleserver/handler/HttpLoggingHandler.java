package com.bxcl.ubi.sampleserver.handler;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

@Slf4j
@Component
@WebFilter(filterName = "httpLoggingHandler", urlPatterns = "/*")
public class HttpLoggingHandler extends HttpFilter {
    private static final long serialVersionUID = -7791168563871425753L;

    @Override
    protected void doFilter(
            HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        ContentCachingRequestWrapper cachingRequestWrapper = new ContentCachingRequestWrapper(request);
        ContentCachingResponseWrapper cachingResponseWrapper =
                new ContentCachingResponseWrapper(response);
        super.doFilter(cachingRequestWrapper, cachingResponseWrapper, chain);
        Enumeration<String> headerNames = request.getHeaderNames();
        List<String> headers = new ArrayList<>();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            headers.add(String.format("%s:%s;", headerName, request.getHeader(headerName)));
        }
        String requestBody =
                new String(cachingRequestWrapper.getContentAsByteArray(), StandardCharsets.UTF_8);
        String responseBody =
                new String(cachingResponseWrapper.getContentAsByteArray(), StandardCharsets.UTF_8);
        log.info(
                "request: remote={} method={} uri={} headers={{}} params={{}} | body={} response: status={}, body={}",
                request.getRemoteAddr(),
                request.getMethod(),
                request.getRequestURI(),
                String.join(" ", headers),
                request.getQueryString(),
                requestBody,
                response.getStatus(),
                responseBody);
        cachingResponseWrapper.copyBodyToResponse();
    }
}

